<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Article;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        /**
         * Create root user.
         */
        $user = new User();
        $user->first_name = 'Michał';
        $user->last_name = 'Jendraszczyk';
        $user->email = 'michal.jendraszczyk@gmail.com';
        $user->password = bcrypt('1');
        $user->gender = 'boy';
        $user->is_active = '1';
        $user->save();
        /**
         * Create simple articles.
         */
        $contents = [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dignissim eros ut ligula vehicula, ac ullamcorper sem bibendum. Integer felis nibh, efficitur at tristique vel, feugiat sed erat. In maximus dignissim ligula, sed lacinia lectus maximus non. Nunc arcu purus, accumsan non justo ac, facilisis suscipit justo. Maecenas ac turpis luctus, vulputate leo at, bibendum purus. Cras sed dolor vitae ex ornare viverra. Nunc hendrerit odio a tortor hendrerit, nec congue turpis dapibus. Praesent varius quis dui blandit maximus.
        Etiam ultrices velit quis magna convallis egestas. Vivamus quis urna sed tellus dignissim vestibulum. Cras ornare tellus nec vestibulum dapibus. Ut lorem augue, scelerisque sit amet erat eget, pharetra placerat diam. Maecenas finibus ligula ut sapien iaculis, id rhoncus dui convallis. Nulla in elit erat. Ut vehicula malesuada purus a euismod. Donec sit amet varius libero. In hac habitasse platea dictumst. Fusce vulputate, quam ut molestie efficitur, sapien massa rhoncus metus, a pharetra libero tortor vitae nulla. Etiam aliquam urna a lobortis consequat.',

        'Fusce vitae ante tincidunt, imperdiet erat finibus, lobortis lectus. Aenean accumsan dapibus metus, et accumsan ex lobortis nec. Nullam in euismod nulla. Morbi suscipit interdum nisi, eget vehicula dolor sollicitudin sit amet. Sed sed diam hendrerit, consequat quam vitae, laoreet mi. Mauris scelerisque quam nisi, vel tempor tellus rutrum vitae. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce a aliquet nisl.
        ',

        'Pellentesque varius augue et lorem fringilla, ac tristique magna eleifend. Duis in tortor semper, molestie velit at, fringilla ligula. Aenean pretium laoreet mi, id dictum urna gravida at. Vivamus purus elit, fringilla eget rutrum sit amet, tempus vel nulla. Pellentesque tristique mattis tellus vitae lacinia. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque vitae ipsum in enim tincidunt malesuada. Phasellus ut ligula cursus, lobortis lacus at, elementum ligula. Cras sit amet sem scelerisque, finibus dolor faucibus, hendrerit nunc. Fusce interdum, purus dapibus suscipit auctor, lectus augue ultricies lectus, quis vulputate turpis felis et nulla. Donec at libero dapibus, dapibus sem sit amet, tristique magna.
        ',
        ];
        for ($i = 0; $i < 3; ++$i) {
            $article = new Article();
            $article->name = Str::substr($contents[$i], 0, 30);
            $article->description = Str::substr($contents[$i], 0, 190);
            $article->is_active = '1';
            $article->user_id = '1';
            $article->save();
        }
    }
}
