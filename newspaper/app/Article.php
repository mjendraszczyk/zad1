<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'is_active', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
