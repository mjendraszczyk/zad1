<?php

namespace App\Http\Controllers;

use App\Article;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = (new Article())->with('user')->limit(5)->orderBy('id', 'desc')->get();

        return view('index')->with('articles', $articles);
    }
}
