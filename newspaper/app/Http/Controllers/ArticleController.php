<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Auth;
use App\Http\Middleware\IsActive;

class ArticleController extends Controller
{
    public $activies;

    public function __construct()
    {
        $this->middleware(['auth', IsActive::class]);
        $this->activies = ['Niektywny' => '0', 'Aktywny' => '1'];
    }

    public function index()
    {
        $articles = (new Article())->orderBy('id', 'desc')->get();

        return view('articles.index')->with('articles', $articles)->with('activies', $this->activies);
    }

    public function create()
    {
        return view('articles.create')->with('activies', $this->activies);
    }

    public function edit($id)
    {
        $articles = Article::where('id', $id)->get();

        return view('articles.edit')->with('articles', $articles)->with('activies', $this->activies);
    }

    public function store(Request $request)
    {
        $request->validate([
             'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string', 'max:191'],
            'is_active' => ['required'],
        ]);

        $article = new Article();

        $article->name = $request->name;
        $article->description = $request->description;
        $article->updated_at = date('Y-m-d H:i:s');
        $article->created_at = date('Y-m-d H:i:s');
        $article->is_active = $request->is_active;
        $article->user_id = Auth::id();
        $article->save();

        $request->session()->flash('status', 'Dodano pomyslnie!');

        return redirect(route('articles.index'));
    }

    public function show($id)
    {
    }

    public function update(Request $request, $id)
    {
        $request->validate([
             'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string', 'max:191'],
            'is_active' => ['required'],
        ]);

        $article = Article::where('id', $id);

        $article->update([
        'name' => $request->name,
        'description' => $request->description,
        'updated_at' => date('Y-m-d H:i:s'),
        'is_active' => $request->is_active,
        'user_id' => Auth::id(),
        ]);

        $request->session()->flash('status', 'Zapisano pomyslnie!');

        return redirect(route('articles.edit', $id));
    }

    public function destroy($id)
    {
        Article::where('id', $id)->delete();

        return redirect()->back();
    }
}
