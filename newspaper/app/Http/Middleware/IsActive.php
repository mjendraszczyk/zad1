<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = \Auth::id();

        $user = User::find($user_id);
        // dd($user);

        if ($user->is_active == '0') {
            abort(403, 'Unauthorized user');
        } else {
            return $next($request);
        }
    }
}
