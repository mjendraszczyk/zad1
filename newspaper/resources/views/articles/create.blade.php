@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Edycja - Artykuły</h1>
<div class="card">
      <div class="card-body">
          <form action="{{route('articles.store')}}" method="POST">
                         @if(Session::has('status'))
           <div class="alert alert-success">
           {{ Session::get('status') }}
           </div>
           @endif
                @if(count($errors->all())>0)
           <div class="alert alert-danger">
           @foreach($errors->all() as $error) 
                {{$error}}<br/>
           @endforeach
</div>
           @endif
           @csrf
          @include('articles._form')
          </form>
</div>
</div>
</div>
@endsection