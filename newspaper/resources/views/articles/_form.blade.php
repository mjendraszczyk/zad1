<label>Name</label>
<input class="form-control" value="@if(Route::currentRouteName() == 'articles.edit'){{$article->name}}@endif" name="name">
<label>Desc</label>
<textarea class="form-control" name="description">@if(Route::currentRouteName() == 'articles.edit'){{$article->description}}@endif</textarea>
<label>Active</label>
<!-- {{print_r($activies)}} -->
   <select class="form-control" name="is_active">
        @foreach($activies as $key => $active)
        @if(Route::currentRouteName() == 'articles.edit')
        <option value="{{$active}}" @if($article->is_active == $active) selected="selected"  @endif>{{$key}}</option>
        @else
                <option value="{{$active}}">{{$key}}</option>

        @endif
        @endforeach

      </select>  
<div class="clearfix"></div>
<input type="submit" class="btn btn-primary" />
