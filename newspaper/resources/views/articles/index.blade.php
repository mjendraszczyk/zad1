@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Artykuły <a href="{{route('articles.create')}}" class="btn btn-success">Nowy</a></h1>
    
     
    <table class="table table-striped">
        <thead class="thead-white">
            <tr>
            <th scope="col">
                #
            </th>
            <th scope="col">
            name
            </th>
            <th scope="col">
                desc
</th>
            <th scope="col">
     is_active
</th>
            <th scope="col">
                options
            </th>
            </tr>
        </thead>
        <tbody>
        @foreach($articles as $key => $article) 
    <tr>
        <td>
{{(int)$key+1}}
        </td>
        <td>
{{$article->name}}
        </td>
        <td>
{{$article->description}}
        </td>
        <td>
            {{$article->is_active}}
        </td>
        <td>
            <a class="btn btn-link" href="{{route('articles.edit',['id'=>$article->id])}}">
                edytuj
            </a>

                   <form action="{{route('articles.destroy',['id'=>$article->id])}}" method="POST">
            @method('delete')
           @csrf
          
<input type="submit" value="usuń" class="btn btn-link" />

</form>
        </td>
    </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection