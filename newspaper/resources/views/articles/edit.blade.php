@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Edycja - Artykuły</h1>
    <div class="card">
      <div class="card-body">
           @if(Session::has('status'))
           <div class="alert alert-success">
           {{ Session::get('status') }}
           </div>
           @endif
           @if(count($errors->all())>0)
           <div class="alert alert-danger">
           @foreach($errors->all() as $error) 
                {{$error}}
           @endforeach
</div>
           @endif
          @foreach($articles as $article)
          <form action="{{route('articles.update',['id'=>$article->id])}}" method="POST">
            @method('patch')
           @csrf
          
@include('articles._form')

</form>
@endforeach
</div>
</div>
</div>
@endsection