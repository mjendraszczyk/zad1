@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        @foreach($articles as $article)
        @if($article->is_active == '1')
            <div class="card">
                <div class="card-header"><h3 class="card-title">{{$article->name}}</h3></div>
                <div class="card-body">
   
                {{$article->description}}
                </div>
                <div class="card-footer text-muted">
    Utworzono: {{$article->created_at}} przez {{$article['user']->first_name}}
  </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
@endsection
